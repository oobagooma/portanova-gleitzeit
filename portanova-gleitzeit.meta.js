// ==UserScript==
// @name         gjm: portanova: gleitzeit additions
// @namespace    oobagooma@gmail.com
// @description  Munge gleitzeit page
// @include      https://portanova.aeat.allianz.at/portanova/authsec/portal/default/gleitzeit-popup*
// @include      https://portanova.aeat.allianz.at/portanova/portal/default/gleitzeit-popup*
// @updateURL    https://bitbucket.org/oobagooma/portanova-gleitzeit/raw/master/portanova-gleitzeit.meta.js
// @downloadURL    https://bitbucket.org/oobagooma/portanova-gleitzeit/raw/master/portanova-gleitzeit.user.js
// @require      https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js
// @require      https://userscripts.org/scripts/source/145813.user.js
// @grant        none
// @version      1.5.7
// ==/UserScript==
