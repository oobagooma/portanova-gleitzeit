// ==UserScript==
// @name         gjm: portanova: gleitzeit preload script
// @namespace    oobagooma@gmail.com
// @description  gleitzeit-pre
// @include      https://portanova.aeat.allianz.at/portanova/authsec/portal/default/gleitzeit-popup*
// @include      https://portanova.aeat.allianz.at/portanova/portal/default/gleitzeit-popup*
// @updateURL    https://bitbucket.org/oobagooma/portanova-gleitzeit/raw/master/portanova-gleitzeit-pre.meta.js
// @downloadURL    https://bitbucket.org/oobagooma/portanova-gleitzeit/raw/master/portanova-gleitzeit-pre.user.js
// @version      1.4
// @run-at       document-start
// @grant        GM_addStyle
// @grant        GM_log
// ==/UserScript==
