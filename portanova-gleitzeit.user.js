// ==UserScript==
// @name         gjm: portanova: gleitzeit additions
// @namespace    oobagooma@gmail.com
// @description  Munge gleitzeit page
// @include      https://portanova.aeat.allianz.at/portanova/authsec/portal/default/gleitzeit-popup*
// @include      https://portanova.aeat.allianz.at/portanova/portal/default/gleitzeit-popup*
// @updateURL    https://bitbucket.org/oobagooma/portanova-gleitzeit/raw/master/portanova-gleitzeit.meta.js
// @downloadURL    https://bitbucket.org/oobagooma/portanova-gleitzeit/raw/master/portanova-gleitzeit.user.js
// @require      https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js
// @require      https://userscripts.org/scripts/source/145813.user.js
// @grant        none
// @version      1.5.8
// ==/UserScript==
lang = "en";

//'use strict';
// @xgrantx        GM_addStyle
// @xgrantx        GM_log
// @xgrantx        GM_getValue
// @xgrantx        GM_setValue
// @xgrantx        GM_info

/*
 * From: http://wiki.greasespot.net/XPath_Helper
 */ 
function $x() {
	var x='';
	var node=document;
	var type=0;
	var fix=true;
	var i=0;
	var cur;

	function toArray(xp) {
		var final=[], next;
		while (next=xp.iterateNext()) {
			final.push(next);
		}
		return final;
	}

		while (cur=arguments[i++]) {
		switch (typeof cur) {
			case "string": x+=(x=='') ? cur : " | " + cur; continue;
			case "number": type=cur; continue;
			case "object": node=cur; continue;
			case "boolean": fix=cur; continue;
		}
	}

	if (fix) {
		if (type==6) type=4;
		if (type==7) type=5;
	}

	// selection mistake helper
	if (!/^\//.test(x)) x="//"+x;
	
	// context mistake helper
	if (node!=document && !/^\./.test(x)) x="."+x;
	
	var result=document.evaluate(x, node, null, type, null);
	if (fix) {
	// automatically return special type
		switch (type) {
			case 1: return result.numberValue;
			case 2: return result.stringValue;
			case 3: return result.booleanValue;
			case 8:
			case 9: return result.singleNodeValue;
		}
	}
	
	return fix ? toArray(result) : result;
} /* end $x() */

function debug() {
	bugging=true;
	str=Array.prototype.slice.call(arguments).join(' :: ');
	if(bugging) {
		console.log(str);
	}
}

function insertfudge(firstday,lastday,dateobj,ins_bef_tr,hrs_delta) {
	var tmpdobj=new Date(dateobj);
	var delta_diff=0;
	var default_mode='holi'; //-7.7, bunk: no show/not present ; 0: holiday
	for(x=firstday;x<=lastday;x++) {
		tmpdobj.setDate(x);
		if(tmpdobj.getDay()>0 && tmpdobj.getDay()<6) {
			debug("Spreading fudge for "+x);
			let pref='gleitzeit_'+tmpdobj.toLocaleFormat('%Y%m%d');
			let mode=GM_getValue(pref,default_mode);
			console.log("Mode -> " + pref + " -> " + mode);
			if(mode=='holi') {
//				alert('holi');
				var hrs_soll='0.00';
				var hrs_ist='0.00';
				var hrs_diff=0;
			} else {
//				alert('bunk');
				var hrs_soll='7.70';
				var hrs_ist='0.00';
				var hrs_diff=-7.70;
			}
			delta_diff+=hrs_diff;
			tmp=document.createElement('tr');
//						'var tr=this.parentNode.parentNode.parentNode;tr.childNodes[9].innerHTML=""; /*soll*/	tr.childNodes[13].innerHTML="7.70"; /*ist*/ tr.childNodes[17].innerHTML="0.00"; /*diff*/ GM_setValue("', tmpdobj.toLocaleFormat('%Y%M%D'), '","holi"); recalc();return false; ',
			tmp.innerHTML='<td width="5" height="20" bgcolor=""></td><td align="right">&nbsp;' + dow[tmpdobj.getDay()] + '.&nbsp;' + tmpdobj.toLocaleFormat('%d.%m.%Y') + '</td><td align="center"><a onclick="' +
						'var tr=this.parentNode.parentNode;tr.childNodes[4].innerHTML=' + "'0.00'" + '; /*soll*/ tr.childNodes[6].innerHTML=' + "'0.00'" + '; /*ist*/ tr.childNodes[8].innerHTML=' + "'0.00'" + '; /*diff*/ GM_setValue(' + "'" + pref + "', 'holi'" + '); recalc();return false;' +
						'">Feiertag?</a></td><td align="center"><a onclick="' +
						'var tr=this.parentNode.parentNode;tr.childNodes[4].innerHTML=' + "'7.70'" + '; /*soll*/ tr.childNodes[6].innerHTML=' + "'0.00'" + '; /*ist*/ tr.childNodes[8].innerHTML=' + "'<span class=&#34;negative&#34;>-7.70</span>'" + '; /*diff*/ GM_setValue('+"'" + pref + "', 'bunk'" + '); recalc(); return false;' +
						'">Quartalstag?</a></td>' +
						'<td width="55" align="right">'+hrs_soll+'</td><td width="25"></td><td width="55" align="right">'+hrs_ist+'</td><td width="25"></td>' +
						'<td width="55" align="right">'+wrapfloat(hrs_diff)+'</td>' +
						'<td width="25"></td><td width="55" align="right"><!-- 0.00 --></td><td width="25"></td><td width="55" align="right"><!-- 0.00 --></td><td width="25"></td>' +
						'<td width="150" bgcolor="#F2F2EC" align="left"></td><td align="right">' + wrapfloat(hrs_delta+delta_diff) +'&nbsp;</td>';
						//'<td width="150" bgcolor="#F2F2EC" align="left"></td><td align="right">' + (hrs_delta!=0?wrapfloat(hrs_delta+delta_diff):'') +'&nbsp;</td>';
			tmp.setAttribute('class','fudge entry'+(tmpdobj.getDay===1?' today':'')+(tmpdobj.getDay()===1?' monday':''));
			ins_bef_tr.parentNode.insertBefore(tmp,ins_bef_tr);

		}
	}
	return delta_diff;
}


/* l=GM_listValues();
for(i=0;i<l.length;i++) {
	console.log("STORAGE: "+l[i]+"="+GM_getValue(l[i]));
	//if(l[i]!='gleitzeit_today' && l[i]!='gleitzeit_came_at') {
	//	GM_deleteValue(l[i]);
	//}
}*/
try { 
	//document.normalize();
	//trs=$x('//div[@id="zeitaufzeichnungDIV"]/table[1]/tbody/tr', XPathResult.ORDERED_NODE_SNAPSHOT_TYPE),i=0,tmp='',running=0;
	trs=$x('//div[@id="zeitaufzeichnungDIV"]/table[1]/tbody/tr/td[2][contains(text(),".")]/..', XPathResult.ORDERED_NODE_SNAPSHOT_TYPE),i=0,tmp='',running=0;
	selectedDate=document.forms["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm"].elements["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"].options[document.forms["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm"].elements["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"].selectedIndex].value; //YYYYMM (MM=00-11; so inc it)
	selectedDate++;
	curDate=(new Date()).toLocaleFormat('%Y%m');
	make_month_nextprev_links();
	if(trs.length===0 && selectedDate==curDate) {
		console.log("first day of month: do fudge & today!");
/*
<div id="zeitaufzeichnungDIV" style="position:relative;top:0px;left:0px;width:100%; height:100%; overflow:scroll; overflow-x:hidden; overflow-y:auto; scrollbar-arrow-color:#888855; scrollbar-base-color:#FFFFFF; scrollbar-track-color:#FFFFFF; scrollbar-face-color:#E7E6D7; scrollbar-highlight-color:#FFFFFF; scrollbar-3dlight-color:#FFFFFF; scrollbar-darkshadow-color:#FFFFFF; scrollbar-shadow-color:#FFFFFF">
					<table border="0" cellpadding="0" cellspacing="0" style="font-family:Verdana; font-weight:normal; font-size:12px;">
						<tr>
							<td width="5"></td>
							<td width="85">Tag</td>
							<td width="80" align="center">Kommt</td>
							<td width="80" align="center">Geht</td>
							<td colspan="2" align="center">Soll</td>
							<td colspan="2" align="center">Ist</td>
							<td colspan="2" align="center">+/-</td>
							<td colspan="2" align="center">Üstbez</td>
							<td colspan="2" align="center">ÜstZAG</td>
							<td></td>
						</tr>
						<tr>
							<td height="15" colspan="15"></td>
						</tr>
*/


		//hmmm - we'd need to fudge the complete #zeitaufzeichnungDIV div + table... 
	}else if(trs.length>0) { //are there any entries for this month? (otherwise: Für diesen Monat existiert noch keine Aufzeichnung! )
		if(lang==="en") {
			dow=new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
		} else {
			dow=new Array('So','Mo','Di','Mi','Do','Fr','Sa');
		}
		tmp=document.createElement('td');
		tmp.setAttribute('align','right');
		tmp.innerHTML='Delta&nbsp;';
		trs[0].parentNode.firstChild.appendChild(tmp);
		lastdatestr='';lastdom=0;
		//todo: check (before for loop) for missing entries:::
		/* firstentry>1 && firstworkingday==monday & dayofmonth<=3 //1st at the weekend
		*/
		{ //prefudge
			var fe_str=trs[0].children[1].innerHTML;
			var fe_date=new Date(fe_str.substring(6,10),(fe_str.substring(3,5)-1),fe_str.substring(0,2));
			if(!(fe_date.getDate()===1 || //first day of month, fine...
			  (fe_date.getDate()<=3 && fe_date.getDay()===1))) { //first day of month was at weekend, thats also fine
				//if it is not one of those two conditions, then do stuff...
				debug("!OK: fudge until "+fe_date.getDate());
			//} else {
			//	alert("OK: "+fe_date.getDate());
			}

		}
		var date;
		for(i=0,mi=trs.length;i<mi;i++) {
			trclass="entry";
			tr=trs[i];
			var datestr=tr.children[1].innerHTML;
			if(lastdatestr==datestr) { //multiple rows per day; don't repeat date
				tr.children[1].innerHTML='';
			} else {
				date=new Date(datestr.substring(6,10),(datestr.substring(3,5)-1),datestr.substring(0,2));
				tr.children[1].innerHTML="&nbsp;"+dow[date.getDay()]+".&nbsp;"+tr.children[1].innerHTML;
				tr.children[1].setAttribute('align','right');
				if(date.getDay()==1) { //monday
					trclass+=' monday';
					//tr.setAttribute('class','monday');
				}
			}

			if(lastdom<date.getDate()-1 && !(date.getDate()<=3 && date.getDay()==1) && !((lastdom+3)===date.getDate() && date.getDay()===1)) { //middle fudge
				debug("Fudge between "+(lastdom+1)+" and "+(date.getDate()-1));
				running+=insertfudge(lastdom+1,date.getDate()-1,date,tr,running);

			}
			lastdatestr=datestr;
			lastdom=date.getDate();
			{
				let kommstr=tr.children[2].textContent;
				let gehtstr=tr.children[3].textContent;
				let gehthrsn=new Number(gehtstr.substring(0,2));
				let kommhrsn=new Number(kommstr.substring(0,2));
				
				if(kommhrsn>=19 || kommhrsn<7) {
					tr.children[2].setAttribute("class","overtime");
					//tr.children[3].style.class="overtime";
				}
				if(gehthrsn>=19 || gehthrsn<7) {
					tr.children[3].setAttribute("class","overtime");
					//tr.children[3].style.class="overtime";
				}
				console.log(gehtstr+ " xx "+ gehthrsn);
			}


			ist=parseFloat(tr.children[6].innerHTML?tr.children[6].innerHTML:0);
			hrsdiff=parseFloat(tr.children[8].innerHTML?tr.children[8].innerHTML:0);
			//ist>0 && diff=0 when exactly soll hours have been worked (7.7)
			if(hrsdiff===0 && ist===0) { //0 hours worked, diff is also 0 - holiday! 
				tr.children[8].innerHTML=tr.children[8]='';
			} else {
				tr.children[8].innerHTML=wrapfloat(hrsdiff);
			}

			tmp=document.createElement('td');
			tmp.setAttribute('bgcolor',tr.children[8].getAttribute('bgcolor'));
			tmp.setAttribute('align','right');
			running+=hrsdiff;
			if(ist!=0 || tr.children[6].innerHTML) {
				tmp.innerHTML=wrapfloat(running);
			}
			tmp.innerHTML+="&nbsp;";
			tr.appendChild(tmp);
			tr.setAttribute('class',trclass);
		} 
		if(selectedDate<curDate) { //if it is a previous month, check for and handle fudge
			var lastdom=new Date(date); //date==last found entry in table
			lastdom.setDate(1);
			lastdom.setMonth(lastdom.getMonth()+1);
			lastdom.setDate(0); //last day of month
			if(lastdom.getDate()>date.getDate()) {
				//debug("Fudge between "+(date.getDate()+1)+" and "+(lastdom.getDate());
				running+=insertfudge(date.getDate()+1,lastdom.getDate(),date,trs[trs.length-1].nextSibling,running);
			}
		}


		$('div#zeitaufzeichnungDIV tr:eq(2)').addClass('firstentry');
		$('div#zeitaufzeichnungDIV tr td[height="25"][colspan="15"]').parent().prev().addClass('lastentry');
		//trs[0].setAttribute('class',trs[0].getAttribute('class')+' firstentry');
		//tr.setAttribute('class',tr.getAttribute('class')+' lastentry');


		//if we're on the current month, then fudge the values for today
		//displaymonth==yyyymm, where mm===0 - 11 for jan - dec
		displayedmonth=1 + new Number($x('//form[@id="pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm"]//select[@id="pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"]/option[@selected]/@value',XPathResult.STRING_TYPE)); //if no month is selected in dropdown (the default after logging in), then this will be set to 1
		thismonth=new Number((new Date()).toLocaleFormat('%Y%m'));
		if(displayedmonth==1 || displayedmonth==thismonth) {
			//fudge_today();
			tmp=document.createElement('tr');
			tmpdate=new Date();
			tmpdatestr=dow[tmpdate.getDay()]+'.&nbsp;'+tmpdate.toLocaleFormat('%d.%m.%Y');
			tmp.innerHTML=	'<td width="5" height="20" bgcolor=""></td><td bgcolor="#F2F2EC" align="right">&nbsp;'+tmpdatestr+'</td><td bgcolor="#F2F2EC" align="center" id="today-came-at">foo?</td> \
							<td bgcolor="#F2F2EC" align="center" id="today-go">bar?</td><td width="55" bgcolor="#F2F2EC" align="right">7.70</td><td width="25" bgcolor="#F2F2EC"></td> \
							<td width="55" bgcolor="#F2F2EC" align="right" id="today-total">wibble?</td><td width="25" bgcolor="#F2F2EC"></td> \
							<td width="55" bgcolor="#F2F2EC" align="right" id="today-diff"><!-- <span class="positive">wobble?</span> --></td><td width="25" bgcolor="#F2F2EC"></td> \
							<td width="55" bgcolor="#F2F2EC" align="right"><!-- 0.00 --></td><td width="25" bgcolor="#F2F2EC"></td><td width="55" bgcolor="#F2F2EC" align="right"><!-- 0.00 --></td> \
							<td width="25" bgcolor="#F2F2EC"></td><td width="150" bgcolor="#F2F2EC" align="left"></td><td bgcolor="#F2F2EC" align="right" id="today-delta"></td>';
			tmp.setAttribute('id','today');
			tr.parentNode.insertBefore(tmp,tr.nextSibling);
			let stored_today=GM_getValue('gleitzeit_today',false);
			came_at=new Date(new Number(GM_getValue('gleitzeit_came_at')));
			today=(new Date()).toLocaleFormat('%Y%m%d').toString();
			now=new Date();
			if(stored_today!==today) {
				GM_setValue('gleitzeit_today',today);
				GM_setValue('gleitzeit_came_at',now.getTime().toString());
				came_at=now;
			}
			document.getElementById('today-came-at').innerHTML=came_at.toLocaleTimeString();
			calcGoHome();
		}
		sumtd=$x('//td[text()="Summe vor Umbuchung:"]/../td[5]');
		sumtd[0].innerHTML=wrapfloat(parseFloat(sumtd[0].innerHTML));
		sumtd=$x('//td[text()="Umbuchung:"]/../td[5]');
		sumtd[0].innerHTML=wrapfloat(parseFloat(sumtd[0].innerHTML));
		sumtd=$x('//td[text()="Monatssalden:"]/../td[5]');
		sumtd[0].innerHTML=wrapfloat(parseFloat(sumtd[0].innerHTML),true);

		verinfo=$x('//div[@id="FooterWindow"]/table/tbody/tr/td');
		//verinfo=verinfo[0].innerHTML;
		tmp=document.createElement('div');
		tmp.setAttribute('id','copy-ver');
		tmp.innerHTML=verinfo[0].innerHTML+'<br />(gleitzeit additions &copy; <a href="mailto:Gerard.Murphy@Allianz.at" title="Gerard Murphy">gjm</a> 2012 v'+GM_info.script.version+")"; //GM_info['script']['version']
		document.getElementById('ApplicationWindow').appendChild(tmp);
		
		logo=$x('//td[contains(@style,"foto-zeituebersicht.gif")]');
		if(logo) {
			logo[0].setAttribute('onclick','window.location.href="/portanova/portal/default/gleitzeit-popup"');
			logo[0].style.cursor='pointer';
		}
	}
} catch(e) {
	console.log("ouch! you borked it!");
	throw(e);
} finally {
//show the timesheet div, and style it appropriately
	GM_addStyle("div#zeitaufzeichnungDIV tr.entry td:first-child { border-right: 1px solid #333; } \
				div#zeitaufzeichnungDIV tr.entry td:last-child { border-right: 1px solid #333; } \
				div#zeitaufzeichnungDIV span.negative { color: #900; } \
				div#zeitaufzeichnungDIV span.positive { color: #080; } \
				div#zeitaufzeichnungDIV tr.firstentry td + td, div#zeitaufzeichnungDIV tr.monday td + td { border-top: 1px solid #333 } \
				div#zeitaufzeichnungDIV tr.lastentry td + td { border-bottom: 1px solid #333 } \
				div#zeitaufzeichnungDIV { display: block; margin-bottom: 0.5em;} \
				div#zeitaufzeichnungDIV tr#today { color: blue; font-weight: bold; } \
				div#zeitaufzeichnungDIV tr.fudge td+td { background-color: #FFFF66; } \
				div#zeitaufzeichnungDIV tr.fudge td a { font-size: 20%; } \
				div#zeitaufzeichnungDIV tr.fudge td a:hover { text-decoration: underline; } \
				div#copy-ver { position: fixed; padding: 0.2em 0.2em 0.3em 0.3em; left: 0; bottom: 0; font-size: 80%; opacity: 0.2; } \
				div#copy-ver:hover { opacity: 1; background-color: #DDDCD1; border-top: 1px solid black; border-right: 1px solid black;} \
				td.overtime { background-color: #fbb;}\
				");
}


function make_month_nextprev_links() {
	let selectedDate=document.forms["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm"].elements["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"].options[
		document.forms["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm"].elements["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"].selectedIndex
	
	].value; //YYYYMM (MM=00-11; so inc it)
	
	//let selidx=document.forms["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm"].elements["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"].selectedIndex;
	//let sellength=document.forms["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm"].elements["pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"].length;

	//let sel=$("#pbApplicationWindow\\:j_id_jsp_1891803107_0\\:gleitzeitZeit\\:gleitzeitZeitForm\\:monate")[0]
	let sel=$(jq("pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"));
	let sel0=$(jq("pbApplicationWindow:j_id_jsp_1891803107_0:gleitzeitZeit:gleitzeitZeitForm:monate"))[0];
	
	let selidx=sel0.selectedIndex;
	//console.dir(sel);
	let sellength=sel0.length;

	if(selidx>0) {
		sel.after(' <a id="nextmonth">&raquo;</a>');
		$("#nextmonth").click(function() {
			sel.val(sel0.options[selidx-1].value);
			sel0.form.submit();
		});
		//console.log("do next month link");
	}
	if(selidx<sellength-1) {
		sel.before('<a id="prevmonth">&laquo;</a> ');
		$("#prevmonth").click(function() {
			sel.val(sel0.options[selidx+1].value);
			sel0.form.submit();
		});
		//console.log("do prev month link");
	}
}

 function jq(myid) { 
   return '#' + myid.replace(/(:|\.)/g,'\\$1');
 }

function calcGoHome() {
	debug("firing today");
	came_at=new Date(new Number(GM_getValue('gleitzeit_came_at')));
	//alert(came_at);
	now=new Date((new Date()).getTime()+300000); //30000: +5mims
	document.getElementById('today-go').innerHTML=now.toLocaleTimeString();
	diff=((now.getTime()-came_at.getTime())/1000)-1800; //1800: half an hour
	diffstr=(diff/3600).toFixed(2);
	$("#today-total").html(diffstr);
	//document.getElementById('today-total').innerHTML=diffstr;

	today_diff=wrapfloat(diffstr-7.7);
	$("#today-diff").html(today_diff); 
	today_delta=wrapfloat(running+(diffstr-7.7));
	$("#today-delta").html(today_delta+"&nbsp;");

	setTimeout(calcGoHome,60000); //60 sec
	//todo: calc diffs, deltas & totals?
}

function wrapfloat(f,ashhmm) {
	var digits=2,strnum=f.toFixed(digits);
	if(ashhmm) {
		strnum+='<br /><b>('+Math.floor(strnum)+':'+((strnum-Math.floor(strnum))*0.6).toFixed(digits).substring(2)+')</b>';
	}
	if(f<0) {
		return '<span class="negative">'+strnum+'</span>';
	} else if(f>0) {
		return '<span class="positive">+'+strnum+'</span>';
	} else { //0
		return strnum;
	}
}


var do_recalc=function() {
	running=0;
	$('div#zeitaufzeichnungDIV tr td:nth-child(16)').each(function(i,e) {
		diffstr=$(this).parent().children(':nth-child(9)').text();
		if(diffstr.length) {
			//alert($(this).parent().children(':nth-child(9)').text());
			running+=Number(diffstr);
			$(this).html(wrapfloat(running)+"&nbsp;");
		}
	});
//	for(i=0;i<deltas.length-1;i++) { //-1: skip last entry: today
//	}
	//console.log($("deltas[1] :parent"));
	//alert(deltas.item(2).parent().children('td:nth-child(10)'));

	//alert("abc");
}

//alert(do_recalc.toString());




$('head').append('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>');
$('head').append('<script>var recalc='+do_recalc.toString()+'; var wrapfloat='+wrapfloat.toString()+';var GM_setValue='+GM_setValue.toString()+';var GM_getValue='+GM_getValue.toString()+';var __GM_STORAGE_PREFIX="'+__GM_STORAGE_PREFIX+'";</script>');

/*
Note to self:


"""
https://getfirebug.com/logging/

Object inspection

How many times have you hand-written code to dump all of the properties of an 
object, or all of the elements in an HTML fragment? With Firebug, you'll never 
write that code again.

Calling console.dir(object) will log an interactive listing of an object's 
properties, like a miniature version of the DOM tab. Calling 
console.dirxml(element) on any HTML or XML element will print a lovely XML 
outline, like a miniature version of the HTML tab.
"""

http://www.parsely.com/api/tracker.html
<!-- START Parse.ly Include: Standard -->
<div id="parsely-root" style="display: none">
  <a id="parsely-cfg" data-parsely-site="##########"
     href="http://parsely.com">Powered by the Parse.ly Publisher Platform (P3).</a>
</div>
<script>
(function(s, p, d) {
  var h=d.location.protocol, i=p+"-"+s,
      e=d.getElementById(i), r=d.getElementById(p+"-root"),
      u=h==="https:"?"d1z2jf7jlzjs58.cloudfront.net"
      :"static."+p+".com";
  if (e) return;
  e = d.createElement(s); e.id = i; e.async = true;
  e.src = h+"//"+u+"/p.js"; r.appendChild(e);
})("script", "parsely", document);
</script>
<!-- END Parse.ly Include: Standard -->


*/