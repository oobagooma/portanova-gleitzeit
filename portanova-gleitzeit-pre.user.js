// ==UserScript==
// @name         gjm: portanova: gleitzeit preload script
// @namespace    oobagooma@gmail.com
// @description  gleitzeit-pre
// @include      https://portanova.aeat.allianz.at/portanova/authsec/portal/default/gleitzeit-popup*
// @include      https://portanova.aeat.allianz.at/portanova/portal/default/gleitzeit-popup*
// @updateURL    https://bitbucket.org/oobagooma/portanova-gleitzeit/raw/master/portanova-gleitzeit-pre.meta.js
// @downloadURL    https://bitbucket.org/oobagooma/portanova-gleitzeit/raw/master/portanova-gleitzeit-pre.user.js
// @version      1.4
// @run-at       document-start
// @grant        GM_addStyle
// @grant        GM_log
// ==/UserScript==

/*
 * Hide stuff that will be later modified/shown by the main gleitzeit script
 */ 
function fudgeit() {
  //GM_log("fudging/hiding...");
  if(document.getElementsByTagName('head')[0]) {
    GM_addStyle('div#FooterWindow, div#zeitaufzeichnungDIV{display: none;}');
  } else {
    window.setTimeout(fudgeit,100);
  }
}

fudgeit();
