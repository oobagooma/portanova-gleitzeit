#!/bin/bash

echo "check before committing!"
for i in *.user.js ; do 
	OF=$(echo $i|sed 's/user.js$/meta.js/')
	sed  -e '/==\/UserScript==/ q' <$i >$OF ; 
	echo "-- $OF --"
	cat $OF
done
